import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
// import helmet from 'helmet';
import { WrapResponseInterceptor } from './common/interceptors/wrap-response.interceptor';
import { HttpExceptionFilter } from './common/interceptors/http-exception.filter';
import { envObjStart } from './config/envsub.config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { constants } from 'zlib';
async function bootstrap() {
  await envObjStart();
  // const httpsOptions = {
  //   key: fs.readFileSync('/etc/letsencrypt/live/intrixlabs.com/privkey.pem'),
  //   cert: fs.readFileSync('/etc/letsencrypt/live/intrixlabs.com/fullchain.pem'),
  //   allowHTTP1: true

  // };
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter({ logger: true, bodyLimit: 10048576 }),
  );
  const configService = app.get(ConfigService);
  app.enableCors({ origin: true });
  const logger = new Logger('App');
  // security
  //app.use(helmet());
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  app.register(require('@fastify/multipart'));
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  app.register(require('@fastify/compress'), {
    brotliOptions: { params: { [constants.BROTLI_PARAM_QUALITY]: 4 } },
  });
  const PORT = configService.get('http.port') || 3000;
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalInterceptors(new WrapResponseInterceptor());
  // initial for documentation API
  const config = new DocumentBuilder()
    .setTitle('Sales Force')
    .setDescription('API docs backend for frontend.')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  await app.listen(PORT, '0.0.0.0');
  logger.log(`Application started on port ${PORT}`);
}
bootstrap();
