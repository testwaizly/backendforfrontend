import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { ConfigService } from '@nestjs/config';
import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { AuthService } from 'src/auth/auth.service';
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(
    private authService: AuthService,
    private readonly configService: ConfigService,
  ) {
    super({
      usernameField: 'email',
      passwordField: 'password',
      secretOrKey: configService.get('jwt.access_token_secret'),
    });
  }

  async validate(email: string, password: string): Promise<any> {
    const user = await this.authService.validateUser(email);

    if (!user) {
      throw new UnauthorizedException('user not found');
    }
    const passwordValid = await bcrypt.compare(password, user.password);
    if (user.status === '0') {
      throw new UnauthorizedException('user tidak aktif');
    }
    if (user && passwordValid) {
      return user;
    } else {
      throw new HttpException('Wrong Password', HttpStatus.UNAUTHORIZED);
    }
  }
}
