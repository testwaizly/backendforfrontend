import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Request } from 'express';
import { ExtractJwt, Strategy } from 'passport-jwt';
import path from 'path';
import { RefreshTokenIdsStorage } from 'src/RefreshTokenIdsStorage';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
    private readonly redisService: RefreshTokenIdsStorage,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromAuthHeaderAsBearerToken(),
      ]),
      ignoreExpiration: false,
      secretOrKey: configService.get('jwt.access_token_secret'),
      passReqToCallback: true,
    });
  }

  async validate(request: Request) {
    const log = new Logger('jwtToken');
    let userData; // Deklarasikan variabel userData di sini
    try {
      const accessToken = request?.headers?.authorization.split(' ')[1];
      userData = await this.jwtService.verify(accessToken, {
        secret: this.configService.get('jwt.access_token_secret'),
      });
      const data = await this.redisService.validate(userData.sub);
      console.log(data);
      if (data == null) {
        throw new HttpException('invalid token', HttpStatus.UNAUTHORIZED);
      }
      log.debug('pass verify jwt token');
      return userData;
    } catch (err) {
      console.log(err, 'sasadf');
      if (err.statusCode == 401) {
        console.log('asasa');
        this.redisService.invalidate(userData.sub);
      }
      log.debug(err.status + 'asasaf');
      throw new HttpException(err, err.status);
    }
  }
}
