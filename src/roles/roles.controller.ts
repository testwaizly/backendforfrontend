import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { RolesService } from './roles.service';
import { ApiBearerAuth, ApiBody, ApiQuery, ApiTags } from '@nestjs/swagger';
import { PaginationRolesDTO, RolesDTO } from './dto/roles.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('v1/roles')
@ApiTags('Roles')
export class RolesController {
  constructor(private rolesService: RolesService) {}
  @ApiBearerAuth()
  @ApiQuery({ type: PaginationRolesDTO })
  @UseGuards(AuthGuard('jwt'))
  @Get('/')
  async getCompanies(@Query() query) {
    const data = await this.rolesService.getRoles(query);
    return data;
  }
  @ApiBearerAuth()
  @ApiBody({ type: RolesDTO })
  @UseGuards(AuthGuard('jwt'))
  @Post('/')
  async createRoles(@Body() body: RolesDTO) {
    const data = await this.rolesService.createRoles(body);
    return data;
  }

  @ApiBearerAuth()
  @ApiBody({ type: RolesDTO })
  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  async updateRoles(@Body() body: RolesDTO, @Param('id') id: number) {
    const data = await this.rolesService.updateRoles(body, id);
    return data;
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async deleteCompanies(@Param('id') id: number) {
    const data = await this.rolesService.deleteRoles(id);
    return data;
  }
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async getSingleCompanies(@Param('id') id: number) {
    const data = await this.rolesService.getSingleRoles(id);
    return data;
  }
}
