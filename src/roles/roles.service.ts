import { HttpException, Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class RolesService {
  constructor(@Inject('MASTERDATA_SERVICE') private masterdata: ClientProxy) {}
  async getRoles(query) {
    try {
      const offset = parseInt(query.perPage) * (parseInt(query.page) - 1);
      const items = await firstValueFrom(
        this.masterdata.send('masterdata-get-roles', {
          from: offset,
          total: parseInt(query.perPage),
          search: query.search,
        }),
      );
      const totalPages = Math.ceil(items['total'] / parseInt(query.perPage));
      if (offset || totalPages) {
        return {
          previousPage:
            parseInt(query.page) - 1 ? parseInt(query.page) - 1 : null,
          currentpages: query.page,
          nextPage:
            totalPages > parseInt(query.page) ? parseInt(query.page) + 1 : null,
          total: items['total'],
          totalPages: totalPages,
          items: items['data'],
        };
      } else {
        return { items: items['data'] };
      }
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async createRoles(body) {
    try {
      return await firstValueFrom(
        this.masterdata.send('masterdata-create-roles', body),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async deleteRoles(id) {
    try {
      return await firstValueFrom(
        this.masterdata.send('masterdata-delete-roles', id),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async getSingleRoles(id) {
    try {
      return await firstValueFrom(
        this.masterdata.send('masterdata-get-single-roles', id),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async updateRoles(body, id) {
    try {
      const datarequest = {
        ...body,
        id: id,
      };
      return await firstValueFrom(
        this.masterdata.send('masterdata-update-roles', datarequest),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
}
