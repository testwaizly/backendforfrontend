import { ApiProperty } from "@nestjs/swagger"
import { IsNotEmpty, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';
export class RolesDTO{
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    code:string

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    name:string
}
export class PaginationRolesDTO{
    @ApiProperty()
    page:number

    @ApiProperty()
    perPage:number
}