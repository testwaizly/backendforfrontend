import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  Request,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { ApiBearerAuth, ApiBody, ApiQuery, ApiTags } from '@nestjs/swagger';
import { ChangeStatusDTO, PaginationUsersDTO, UsersDTO } from './dto/users.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('v1/users')
@ApiTags('Users')
export class UsersController {
  constructor(private usersService: UsersService) {}
  @ApiBearerAuth()
  @ApiQuery({ type: PaginationUsersDTO })
  @UseGuards(AuthGuard('jwt'))
  @Get('/')
  async getUsers(@Query() query, @Request() req) {
    const data = await this.usersService.getUsers(query, req);
    return data;
  }
  @ApiBearerAuth()
  @ApiBody({ type: UsersDTO })
  // @UseGuards(AuthGuard('jwt'))
  @Post('/')
  async createUsers(@Body() body: UsersDTO) {
    const data = await this.usersService.createUsers(body);
    return data;
  }

  @ApiBearerAuth()
  @ApiBody({ type: UsersDTO })
  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  async updateUsers(@Body() body: any, @Param('id') id: number) {
    const data = await this.usersService.updateUsers(body, id);
    return data;
  }

  @ApiBearerAuth()
  @ApiBody({ type: ChangeStatusDTO })
  @UseGuards(AuthGuard('jwt'))
  @Put(':id/change-status')
  async changeStatus(
    @Body() body: any,
    @Param('id') id: number,
    @Request() req,
  ) {
    const data = await this.usersService.changeStatus(body, id, req);
    return data;
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async deleteCompanies(@Param('id') id: number) {
    const data = await this.usersService.deleteUsers(id);
    return data;
  }
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async getSingleCompanies(@Param('id') id: number) {
    const data = await this.usersService.getSingleUsers(id);
    return data;
  }
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('users-details')
  async getSingleUsersbyUserid(@Request() req) {
    const data = await this.usersService.getSingleUsersByUsersId(req);
    return data;
  }
}
