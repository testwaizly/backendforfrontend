import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
@Injectable()
export class UsersService {
  constructor(
    @Inject('MASTERDATA_SERVICE') private masterdata: ClientProxy,
    private jwtService: JwtService,
  ) {}
  async getUsers(query, req) {
    try {
      const offset = parseInt(query.perPage) * (parseInt(query.page) - 1);
      const token = req.headers['authorization'].split(' ')[1];
      const decodedJwtAccessToken = this.jwtService.decode(token);
      const datauser = JSON.parse(JSON.stringify(decodedJwtAccessToken));
      const items = await firstValueFrom(
        this.masterdata.send('masterdata-get-users', {
          from: offset,
          total: parseInt(query.perPage),
          code: datauser.role.code,
          id_companies: datauser.id_companies,
          search: query.search,
        }),
      );
      const totalPages = Math.ceil(items['total'] / parseInt(query.perPage));
      if (offset || totalPages) {
        return {
          previousPage:
            parseInt(query.page) - 1 ? parseInt(query.page) - 1 : null,
          currentpages: query.page,
          nextPage:
            totalPages > parseInt(query.page) ? parseInt(query.page) + 1 : null,
          total: items['total'],
          totalPages: totalPages,
          items: items['data'],
        };
      } else {
        return { items: items['data'] };
      }
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async createUsers(body) {
    try {
      const saltOrRounds = 10;
      const hashedPassword = await bcrypt.hash(body.password, saltOrRounds);
      body.password = hashedPassword;
      body.is_active = 1;
      body.is_login = 0;
      const roles = await firstValueFrom(
        this.masterdata.send('masterdata-search-roles', { code: body.code }),
      );
      body.id_roles = roles.id_roles;
      const bodyrequest = {
        ...body,
      };
      return await firstValueFrom(
        this.masterdata.send('masterdata-create-users', bodyrequest),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async deleteUsers(id) {
    try {
      return await firstValueFrom(
        this.masterdata.send('masterdata-delete-users', id),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async getSingleUsers(id) {
    try {
      return await firstValueFrom(
        this.masterdata.send('masterdata-get-single-users', id),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async getSingleUsersByUsersId(req) {
    try {
      const token = req.headers['authorization'].split(' ')[1];
      const decodedJwtAccessToken = this.jwtService.decode(token);
      const datauser = JSON.parse(JSON.stringify(decodedJwtAccessToken));
      const id = datauser.sub;
      return await firstValueFrom(
        this.masterdata.send('masterdata-get-single-users', id),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async updateUsers(body, id) {
    try {
      const datarequest = {
        ...body,
        id: id,
      };
      return await firstValueFrom(
        this.masterdata.send('masterdata-update-users', datarequest),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async changeStatus(body, id, req) {
    try {
      const token = req.headers['authorization'].split(' ')[1];
      const decodedJwtAccessToken = this.jwtService.decode(token);
      const datauser = JSON.parse(JSON.stringify(decodedJwtAccessToken));
      if (datauser.sub == id) {
        // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
        throw new HttpException('FORBIDDEN', HttpStatus.FORBIDDEN);
      }
      const datarequest = {
        ...body,
        id: id,
      };
      return await firstValueFrom(
        this.masterdata.send('masterdata-update-users', datarequest),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
}
