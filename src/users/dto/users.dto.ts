import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
export class ChangeStatusDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  is_active: string;
}
export class UsersDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  first_name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  last_name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  code: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  password: string;
}
export class PaginationUsersDTO {
  @ApiProperty()
  page: number;

  @ApiProperty()
  perPage: number;
}
