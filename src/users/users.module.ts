import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { ConfigService } from '@nestjs/config';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { JwtService } from '@nestjs/jwt';

@Module({
  controllers: [UsersController],
  providers: [
    {
      provide: 'MASTERDATA_SERVICE',
      useFactory: (configService: ConfigService) => {
        const user = configService.get('rabbitmq.user');
        const password = configService.get('rabbitmq.password');
        const host = configService.get('rabbitmq.host');
        const queueName = configService.get('service.masterdata');
        return ClientProxyFactory.create({
          transport: Transport.RMQ,
          options: {
            urls: [`amqp://${user}:${password}@${host}`],
            persistent: true,
            queue: queueName,
            // noAck: false,
            queueOptions: {
              durable: true,
              queueMode: 'lazy',
              passive: true,
            },
          },
        });
      },
      inject: [ConfigService],
    },
    UsersService,
    JwtService,
  ],
})
export class UsersModule {}
