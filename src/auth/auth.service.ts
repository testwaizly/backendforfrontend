import { HttpException, Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { ClientProxy } from '@nestjs/microservices';
import { RefreshTokenIdsStorage } from 'src/RefreshTokenIdsStorage';
import * as moment from 'moment-timezone';
import { firstValueFrom } from 'rxjs';
import * as bcrypt from 'bcrypt';
@Injectable()
export class AuthService {
  constructor(
    @Inject('MASTERDATA_SERVICE') private masterdata: ClientProxy,
    @Inject('LOG_SERVICE') private log: ClientProxy,
    private refreshToken: RefreshTokenIdsStorage,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}
  async validateUser(email: string): Promise<any> {
    try {
      const user = await firstValueFrom(
        this.masterdata.send('masterdata-get-single-users-by-username', {
          email: email,
          is_active: 1,
        }),
      );
      return user;
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async refreshAccessToken(
    refreshToken: string,
  ): Promise<{ access_token: string }> {
    try {
      const decoded = this.jwtService.verify(refreshToken, {
        secret: this.configService.get('jwt.access_token_secret'),
      });
      const payloadrefresh = {
        name: decoded.name,
        role: decoded.role,
        status: decoded.is_active,
        sub: decoded.sub,
        type: 'access',
      };
      const accessToken = this.jwtService.sign(payloadrefresh, {
        secret: this.configService.get('jwt.access_token_secret'),
        expiresIn: `${this.configService.get('jwt.access_token_expiration')}`,
      });
      this.refreshToken.invalidate(decoded.sub);
      this.refreshToken.insert(
        decoded.sub,
        accessToken,
        this.configService.get('jwt.access_token_expiration'),
      );
      return { access_token: accessToken };
    } catch (error) {
      throw new HttpException(error.messages, 500);
    }
  }
  async register(body) {
    try {
      const saltOrRounds = 10;
      const hashedPassword = await bcrypt.hash(body.password, saltOrRounds);
      body.password = hashedPassword;
      body.is_active = 1;
      body.is_login = 0;
      const roles = await firstValueFrom(
        this.masterdata.send('masterdata-search-roles', { code: body.code }),
      );
      body.id_roles = roles.id_roles;
      const bodyrequest = {
        ...body,
      };
      return await firstValueFrom(
        this.masterdata.send('masterdata-create-users', bodyrequest),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async login(user: any, req: any) {
    try {
      const date = new Date();
      // // Converts the UTC time to a locale specific format, including adjusting for timezone. oke
      const currentDateTimeCentralTimeZone = date.toLocaleString('sv', {
        timeZone: 'Asia/Jakarta',
      });
      const payload = {
        role: user.roles,
        name: user.first_name + ' ' + user.last_name,
        status: user.is_active,
        sub: user.id_users,
        type: 'access',
      };
      const payloadrefresh = {
        name: user.first_name + ' ' + user.last_name,
        role: user.roles,
        status: user.is_active,
        sub: user.id_users,
        type: 'refresh',
      };
      const dateNow = new Date();
      const localDate = moment.tz(dateNow, 'Asia/Jakarta');
      const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
      const userAgent = req.headers['user-agent'];
      const datauser = {
        name: user.first_name + ' ' + user.last_name,
        role: user.roles,
      };
      const datalog = {
        ipaddress: ip,
        user_agent: userAgent,
        user: datauser,
        action: 'Auth',
        messages: 'Melakukan Login ',
        createdAt: localDate,
      };
      const request = [
        firstValueFrom(this.log.send('log-create-useractivity', datalog)),
        this.masterdata.send('masterdata-update-user', {
          action: 'login',
          is_login: 1,
          id: user.id_users,
          lastLogin: currentDateTimeCentralTimeZone,
        }),
      ];
      await Promise.all(request);
      //   delete user.profile.users
      const data = this.jwtService.sign(payload, {
        secret: this.configService.get('jwt.access_token_secret'),
        expiresIn: `${this.configService.get('jwt.access_token_expiration')}`,
      });
      this.refreshToken.insert(
        user.id_users,
        data,
        this.configService.get('jwt.access_token_expiration'),
      );
      return {
        id: user.id_users,
        role: user.roles,
        status: user.status,
        name: user.first_name + ' ' + user.last_name,
        refresh_token: this.jwtService.sign(payloadrefresh, {
          secret: this.configService.get('jwt.access_token_secret'),
          expiresIn: `${this.configService.get(
            'jwt.refresh_token_expiration',
          )}`,
        }),
        access_token: this.jwtService.sign(payload, {
          secret: this.configService.get('jwt.access_token_secret'),
          expiresIn: `${this.configService.get('jwt.access_token_expiration')}`,
        }),
        is_active: user.is_active,
      };
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
}
