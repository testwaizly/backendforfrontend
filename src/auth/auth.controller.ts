import { Controller, Post, UseGuards, Request, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { LoginDTO } from './dto/auth.dto';
import { UsersDTO } from 'src/users/dto/users.dto';

@Controller('v1/auth')
@ApiTags('AUTH')
export class AuthController {
  constructor(private authService: AuthService) {}
  @ApiBody({ type: LoginDTO })
  @UseGuards(AuthGuard('local'))
  @Post('/login')
  async Login(@Request() req) {
    const roles = await this.authService.login(req.user, req);
    return roles;
  }
  @ApiBody({ type: UsersDTO })
  @Post('/register')
  async register(@Body() body: UsersDTO) {
    const roles = await this.authService.register(body);
    return roles;
  }
  // @UseGuards(AuthGuard('jwt'))
  @Post('refresh-token')
  async refreshToken(@Body() refreshTokenDto: any) {
    return this.authService.refreshAccessToken(refreshTokenDto.refresh_token);
  }
}
