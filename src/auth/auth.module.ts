import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { ConfigService } from '@nestjs/config';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { RefreshTokenIdsStorage } from 'src/RefreshTokenIdsStorage';
import { JwtStrategy } from 'src/strategy/jwt.strategy';
import { JwtService } from '@nestjs/jwt';
import { LocalStrategy } from 'src/strategy/local.strategy';

@Module({
  controllers: [AuthController],
  providers: [
    {
      provide: 'MASTERDATA_SERVICE',
      useFactory: (configService: ConfigService) => {
        const user = configService.get('rabbitmq.user');
        const password = configService.get('rabbitmq.password');
        const host = configService.get('rabbitmq.host');
        const queueName = configService.get('service.masterdata');
        return ClientProxyFactory.create({
          transport: Transport.RMQ,
          options: {
            urls: [`amqp://${user}:${password}@${host}`],
            persistent: true,
            queue: queueName,
            // noAck: false,
            queueOptions: {
              durable: true,
              queueMode: 'lazy',
              passive: true,
            },
          },
        });
      },
      inject: [ConfigService],
    },
    {
      provide: 'LOG_SERVICE',
      useFactory: (configService: ConfigService) => {
        const user = configService.get('rabbitmq.user');
        const password = configService.get('rabbitmq.password');
        const host = configService.get('rabbitmq.host');
        const queueName = configService.get('service.log');
        console.log(queueName);
        return ClientProxyFactory.create({
          transport: Transport.RMQ,
          options: {
            urls: [`amqp://${user}:${password}@${host}`],
            persistent: true,
            queue: queueName,
            // noAck: false,
            queueOptions: {
              durable: true,
              queueMode: 'lazy',
              passive: true,
            },
          },
        });
      },
      inject: [ConfigService],
    },
    RefreshTokenIdsStorage,
    JwtStrategy,
    JwtService,
    LocalStrategy,
    AuthService,
  ],
})
export class AuthModule {}
