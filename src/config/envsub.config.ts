import envsub from 'envsub';
import { join } from 'path';

const YAML_CONFIG_FILENAME = './config.yaml';
const {
  PORT,
  RABBITMQ_USER,
  RABBITMQ_PASSWORD,
  RABBITMQ_HOST,
  // jwt
  JWT_SECRET,
  JWT_EXP,
  JWT_EMAIL_SECRET,
  JWT_EMAIL_EXP,
  JWT_REFRESH_EXP,

  //service
  SERVICE_MASTERDATA,
  SERVICE_LOG,

  //redis
  REDIS_HOST,
  REDIS_PORT,
} = process.env;

const templateFile = join(__dirname, YAML_CONFIG_FILENAME);
const outputFile = join(__dirname, YAML_CONFIG_FILENAME);

const options = {
  all: false,
  diff: false,
  envs: [
    // port
    { name: 'PORT', value: PORT },
    //rabbitmq config
    { name: 'RABBITMQ_USER', value: RABBITMQ_USER },
    { name: 'RABBITMQ_PASSWORD', value: RABBITMQ_PASSWORD },
    { name: 'RABBITMQ_HOST', value: RABBITMQ_HOST },
    // jwt
    { name: 'JWT_SECRET', value: JWT_SECRET },
    { name: 'JWT_EXP', value: JWT_EXP },
    { name: 'JWT_EMAIL_SECRET', value: JWT_EMAIL_SECRET },
    { name: 'JWT_EMAIL_EXP', value: JWT_EMAIL_EXP },
    { name: 'JWT_REFRESH_EXP', value: JWT_REFRESH_EXP },
    //service
    { name: 'SERVICE_MASTERDATA', value: SERVICE_MASTERDATA },
    { name: 'SERVICE_LOG', value: SERVICE_LOG },

    //redis
    { name: 'REDIS_HOST', value: REDIS_HOST },
    { name: 'REDIS_PORT', value: REDIS_PORT },
  ],
  envFiles: [join(__dirname, YAML_CONFIG_FILENAME)],
  protect: false,
  syntax: 'default',
  system: true,
};

// create (or overwrite) the output file
export const envObjStart = () =>
  envsub({ templateFile, outputFile, options })
    .then(() => {
      console.log('env-sub has loaded');
    })
    .catch((err: Error) => {
      console.error(err.message);
    });
