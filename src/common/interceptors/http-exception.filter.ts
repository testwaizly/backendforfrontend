import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { FastifyReply } from 'fastify';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<FastifyReply>();
    const status = exception.getStatus();
    const exceptionResponse = exception.getResponse();

    let error: any;
    if (typeof exceptionResponse === 'string') {
      error = { message: exceptionResponse };
    } else {
      error = exceptionResponse as JSON;
    }

    response.status(status).send({
      ...error,
      service: 'backend-sis',
      timestamp: new Date().toISOString(),
    });
  }
}
