import { HttpException, Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
@Injectable()
export class EmployeeService {
  constructor(@Inject('MASTERDATA_SERVICE') private masterdata: ClientProxy) {}
  async getEmployee(query) {
    try {
      const offset = parseInt(query.perPage) * (parseInt(query.page) - 1);
      const items = await firstValueFrom(
        this.masterdata.send('masterdata-get-employee', {
          from: offset,
          total: parseInt(query.perPage),
          search: query.search,
        }),
      );
      const totalPages = Math.ceil(items['total'] / parseInt(query.perPage));
      if (offset || totalPages) {
        return {
          previousPage:
            parseInt(query.page) - 1 ? parseInt(query.page) - 1 : null,
          currentpages: query.page,
          nextPage:
            totalPages > parseInt(query.page) ? parseInt(query.page) + 1 : null,
          total: items['total'],
          totalPages: totalPages,
          items: items['data'],
        };
      } else {
        return { items: items['data'] };
      }
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async createEmployee(body) {
    try {
      return await firstValueFrom(
        this.masterdata.send('masterdata-create-employee', body),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async deleteEmployee(id) {
    try {
      return await firstValueFrom(
        this.masterdata.send('masterdata-delete-employee', id),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async getSingleEmployee(id) {
    try {
      return await firstValueFrom(
        this.masterdata.send('masterdata-get-single-employee', id),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
  async updateEmployee(body, id) {
    try {
      const datarequest = {
        ...body,
        id: id,
      };
      return await firstValueFrom(
        this.masterdata.send('masterdata-update-employee', datarequest),
      );
    } catch (error) {
      // Tangkap kesalahan RPC dan lemparkan kembali sebagai HttpException
      throw new HttpException(error.message, error.statusCode);
    }
  }
}
