import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiBody, ApiQuery, ApiTags } from '@nestjs/swagger';
import { EmployeeService } from './employee.service';
import { EMployeeDTO, PaginationEmployeeDTO } from './dto/employee.dto';
@Controller('v1/employee')
@ApiTags('Employee')
export class EmployeeController {
  constructor(private employeeService: EmployeeService) {}
  @ApiBearerAuth()
  @ApiQuery({ type: PaginationEmployeeDTO })
  @UseGuards(AuthGuard('jwt'))
  @Get('/')
  async getCompanies(@Query() query) {
    const data = await this.employeeService.getEmployee(query);
    return data;
  }
  @ApiBearerAuth()
  @ApiBody({ type: EMployeeDTO })
  @UseGuards(AuthGuard('jwt'))
  @Post('/')
  async createRoles(@Body() body: EMployeeDTO) {
    const data = await this.employeeService.createEmployee(body);
    return data;
  }

  @ApiBearerAuth()
  @ApiBody({ type: EMployeeDTO })
  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  async updateRoles(@Body() body: EMployeeDTO, @Param('id') id: number) {
    const data = await this.employeeService.updateEmployee(body, id);
    return data;
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  async deleteCompanies(@Param('id') id: number) {
    const data = await this.employeeService.deleteEmployee(id);
    return data;
  }
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async getSingleCompanies(@Param('id') id: number) {
    const data = await this.employeeService.getSingleEmployee(id);
    return data;
  }
}
