import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsNumber } from 'class-validator';
export class EMployeeDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  job_title: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  salary: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  departement: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  joined_date: Date;
}
export class PaginationEmployeeDTO {
  @ApiProperty()
  page: number;

  @ApiProperty()
  perPage: number;
}
