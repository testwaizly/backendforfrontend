import {
  Injectable,
  OnApplicationBootstrap,
  OnApplicationShutdown,
} from '@nestjs/common';
import Redis from 'ioredis';
import { ConfigService } from '@nestjs/config';

export class InvalidatedRefreshTokenError extends Error {}

@Injectable()
export class RefreshTokenIdsStorage
  implements OnApplicationBootstrap, OnApplicationShutdown
{
  private redisClient: Redis;
  constructor(private configService: ConfigService) {
    this.redisClient = new Redis({
      host: this.configService.get('redis.host'),
      port: this.configService.get('redis.port'),
    });
  }
  onApplicationBootstrap() {
    this.redisClient = new Redis({
      host: this.configService.get('redis.host'),
      port: this.configService.get('redis.port'),
    });
  }

  onApplicationShutdown(signal?: string) {
    return this.redisClient.quit();
  }

  async insert(
    userId: number,
    tokenId: string,
    timestampTarget: string,
  ): Promise<void> {
    const data = await this.redisClient.set(this.getKey(userId), tokenId);
    console.log(this.getKey(userId), tokenId);
    const ttltarget = this.convertDurationToUnixTimestamp(timestampTarget);
    const currentTimestamp = Math.floor(Date.now() / 1000); // Timestamp UNIX saat ini dalam detik
    const ttlInSeconds = ttltarget - currentTimestamp;
    this.redisClient.expire(this.getKey(userId), ttlInSeconds);
  }
  convertDurationToUnixTimestamp(duration) {
    const durationInSeconds = this.parseDurationToSeconds(duration);
    if (durationInSeconds !== null) {
      const currentTimestamp = Math.floor(Date.now() / 1000); // Timestamp UNIX saat ini dalam detik
      const targetTimestamp = currentTimestamp + durationInSeconds;
      return targetTimestamp;
    } else {
      return null; // Durasi tidak valid
    }
  }
  parseDurationToSeconds(duration) {
    const regex = /^(\d+)([smhd])$/;
    const match = duration.match(regex);
    if (match) {
      const value = parseInt(match[1]);
      const unit = match[2];
      switch (unit) {
        case 's': // detik
          return value;
        case 'm': // menit
          return value * 60;
        case 'h': // jam
          return value * 60 * 60;
        case 'd': // hari
          return value * 60 * 60 * 24;
        default:
          return null; // Durasi tidak valid
      }
    } else {
      return null; // Durasi tidak valid
    }
  }

  async validate(userId: number) {
    const storedId = await this.redisClient.get(this.getKey(userId));
    return storedId;
  }

  async invalidate(userId: number): Promise<void> {
    await this.redisClient.del(this.getKey(userId));
  }

  private getKey(userId: number): string {
    return `user-${userId}`;
  }
}
